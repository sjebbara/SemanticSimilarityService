# -*- coding: utf-8 -*-

""" Use DeepMoji to encode texts into emotional feature vectors.
"""
from __future__ import print_function, division
import json

import numpy
from deepmoji.sentence_tokenizer import SentenceTokenizer
from deepmoji.model_def import deepmoji_feature_encoding
from deepmoji.global_variables import PRETRAINED_PATH, VOCAB_PATH

from flask import Flask, request
from flask import json
from flask import make_response
from werkzeug.exceptions import abort


class SemanticSimilarityService(object):
    def __init__(self, uri, vocab_filepath="../res/vocabulary.json", model_filepath="../res/deepmoji_weights.hdf5",
                 max_sentence_length=30, normalize_scores=False, debug=False):
        self.uri = uri
        self.vocab_filepath = vocab_filepath
        self.model_filepath = model_filepath
        self.normalize_scores = normalize_scores
        self.max_sentence_length = max_sentence_length
        self.debug = debug
        self.model = None
        self.vocabulary = None

        self.flask_application = Flask(__name__)

        @self.flask_application.before_first_request
        def _init_service():
            self._init_service()

        self.flask_application.debug = debug

        self.flask_application.add_url_rule('/', '', lambda: "Semantic Similarity Service is running ...")
        self.flask_application.add_url_rule('/' + uri, uri, self.process_request, methods=["POST"])

    def _init_service(self):
        print("Initialize service ...")
        self._init_vocabulary()
        self._init_model()
        print("Initialization done.")

    def _init_vocabulary(self):
        print('Tokenizing using dictionary from {}'.format(self.vocab_filepath))
        with open(self.vocab_filepath, 'r') as f:
            self.vocabulary = json.load(f)

    def _init_model(self):
        print('Loading model from {}.'.format(self.model_filepath))
        self.model = deepmoji_feature_encoding(self.max_sentence_length, self.model_filepath)

    def _preprocess_texts(self, texts):
        print("Preprocess texts (maxlen: {}) ...".format(self.max_sentence_length))
        st = SentenceTokenizer(self.vocabulary, self.max_sentence_length)
        tokenized, _, _ = st.tokenize_sentences(texts)
        return tokenized

    def _encode_preprocessed_texts(self, preprocessed_texts):
        print('Encode texts ...')
        encodings = self.model.predict(preprocessed_texts)
        return encodings

    def _encode_texts(self, texts):
        preprocessed_texts = self._preprocess_texts(texts)
        encodings = self._encode_preprocessed_texts(preprocessed_texts)
        return encodings

    def _similarity(self, v1, v2):
        v1n = numpy.linalg.norm(v1)
        v2n = numpy.linalg.norm(v2)
        similarity = numpy.dot(v1, v2) / (v1n * v2n)
        return similarity

    def _softmax(self, scores):
        exp_scores = numpy.exp(scores)
        softmax_scores = exp_scores / numpy.sum(exp_scores, keepdims=True)
        return softmax_scores

    def compute_text_similarities(self, query_text, option_texts):
        all_texts = [query_text] + list(option_texts)
        all_encodings = self._encode_texts(all_texts)

        query_encoding = all_encodings[0]
        option_encodings = all_encodings[1:]

        similarities = []
        for option_encoding in option_encodings:
            similarity = self._similarity(query_encoding, option_encoding)
            similarities.append(similarity)

        if self.normalize_scores:
            return self._softmax(similarities)
        else:
            return similarities

    def process_request(self):
        print("Request received")
        request_data = request.get_json()
        if request_data is not None:
            print("Data extracted")
            query_text = request_data["query"]
            option_texts = request_data["options"]

            print("Query: ", query_text)
            print("Options: ", option_texts)

            print("Compute similarities ...")
            similarities = self.compute_text_similarities(query_text, option_texts)
            response_object = ["{:.8f}".format(s) for s in similarities]
            print("Computed_similarities:", similarities)
            print("prepare response ...")
            json_response = json.dumps(response_object)
            response = make_response(json_response)
            response.mimetype = 'application/json'
            print("send response ...")
            return response
        else:
            abort(400)

    def run(self, *args, **kwargs):
        self.flask_application.run(*args, **kwargs)
        print("Service running at: {}".format(self.uri))


if __name__ == "__main__":
    service = SemanticSimilarityService("similarity", max_sentence_length=30, debug=True)
    service.run(host='0.0.0.0', port=8080)
